﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCOMBO
{

    class ComboDefinitions
    {
        public string Description = "";
        public List<ComboDefinition> Combos;
    }
    class ComboDefinition
    {
        public string ComboName="";
        public int ComboPower = 0;
        public float ComboTime = 0;
        public List<ComboNode> ComboStages;

    }
    class ComboNode
    {
        public int ButtonA = 0;
        public int ButtonB = 0;
        public int ButtonC = 0;
        public int ButtonD = 0;
        public int Left = 0;
        public int Right = 0;
        public int Up = 0;
        public int Down = 0;

        public bool Empty = false;

        public float fTime=0;

        public ComboNode(GamePadState State, float fTime)
        {
            SetNode(State, fTime);
        }

        public void SetNode(GamePadState State, float _fTime)
        {
            if (State.Buttons.A == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.A))
            {
                ButtonA = 1;
            }
            if (State.Buttons.B == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.B))
            {
                ButtonB = 1;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.C))
            {
                ButtonC = 1;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                ButtonD = 1;
            }

            if (State.ThumbSticks.Left.X > 0.6f || State.DPad.Right == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                Right = 1;
            }
            if (State.ThumbSticks.Left.X < -0.6f || State.DPad.Left == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                Left = 1;
            }
            if (State.ThumbSticks.Left.Y < -0.6f || State.DPad.Down == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                Down = 1;
            }
            if (State.ThumbSticks.Left.Y > 0.6f || State.DPad.Up == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                Up = 1;
            }

            if (ButtonA == 0
               && ButtonB == 0
               && ButtonC == 0
               && ButtonD == 0
               && Left == 0
               && Right == 0
               && Up == 0
               && Down == 0)
            {
                Empty = true;
            }
            else
                Empty = false;

            fTime = _fTime;
        }

        public bool Compare(ComboNode OtherNode)
        {
            if (ButtonA == OtherNode.ButtonA
                && ButtonB == OtherNode.ButtonB
                && ButtonC == OtherNode.ButtonC
                && ButtonD == OtherNode.ButtonD
                && Up == OtherNode.Up
                && Down == OtherNode.Down
                && Left == OtherNode.Left
                && Right == OtherNode.Right
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    
    }
}
