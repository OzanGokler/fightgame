﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCOMBO
{
    class GamePadStateData
    {
        public ComboNode State;

        public GamePadStateData(ComboNode _State )
        {
            State = _State;
        }

    }
}
