﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCOMBO
{
    class GameStatic
    {

        static CharacterManager TheCharacters;




        public static List<Missile> Missiles;

        public static void Initialise()
        {
            Missiles = new List<Missile>();
            TheCharacters = new CharacterManager();
        }
        public static void LoadContent()
        {
            TheCharacters.LoadContent();
        }

        public static void Update()
        {
            TheCharacters.Update();
            for (int i = 0; i < Missiles.Count;i++ )
            {
                Missiles[i].Update();
            }
        }

        public static void Draw()
        {
            TheCharacters.Draw();
            for (int i = 0; i < Missiles.Count; i++)
            {
                Missiles[i].Draw();
            }
        }
    }
}
