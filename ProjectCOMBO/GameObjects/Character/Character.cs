﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCOMBO
{
    class Character : AnimatedSprite
    {
        public int iPlayerNo = 0;

        List<GamePadStateData> JoypadStates;
        float fComboDuration = 700;
        public ComboDefinitions AllCombos;
        public Vector2 velocity;
        public bool hasJumped; 
        //general moves
        //high kick
        int iHighKickDuration = 0;
        string sHighKickAnimation = "";
        //high punch
        int iHighPunchDuration = 0;
        string sHighPunchAnimation = "";
        //got hit
        int iGotHitDuration = 0;
        string sGotHitAnimation = "";
        //missile
        int iMissileDuration = 0;
        string sMissileAnimation = "";
        //dapduket
        int iDupduketDuration = 0;
        string sDupduketAnimation = "";
        // easypunch
        int iEasyPunchDuration = 0;
        string sEasyPunchAnimation = "";
        // jumpUp
        int iJumpUpDuration = 0;
        string sJumpUpAnimatiın = "";


        public void SetGeneralMoves(List<Moves> PlayerMoves)
        {
            for (int i = 0; i < PlayerMoves.Count; i++)
            {
                string sMoveName = PlayerMoves[i].sMoveName;
                switch (sMoveName)
                {
                    case "highkick":
                        iHighKickDuration = PlayerMoves[i].iDuration;
                        sHighKickAnimation = PlayerMoves[i].sMoveAnimation;
                        break;
                    case "highpunch":
                        iHighPunchDuration = PlayerMoves[i].iDuration;
                        sHighPunchAnimation = PlayerMoves[i].sMoveAnimation;
                        break;
                    case "gothit":
                        iGotHitDuration = PlayerMoves[i].iDuration;
                        sGotHitAnimation = PlayerMoves[i].sMoveAnimation;
                        break;
                    case "missile":
                        iMissileDuration = PlayerMoves[i].iDuration;
                        sMissileAnimation = PlayerMoves[i].sMoveAnimation;
                        break;
                    case "dapduket":
                        iDupduketDuration = PlayerMoves[i].iDuration;
                        sDupduketAnimation = PlayerMoves[i].sMoveAnimation;
                        break;
                    case "easypunch" :
                        iEasyPunchDuration = PlayerMoves[i].iDuration;
                        sEasyPunchAnimation = PlayerMoves[i].sMoveAnimation;
                        break;
                    case "jumpUp" :
                        iJumpUpDuration = PlayerMoves[i].iDuration;
                        sJumpUpAnimatiın = PlayerMoves[i].sMoveAnimation;
                        break;
                }
            }
        }


        CharacterState CurrentState = CharacterState.Idle;
        string sCurrentActionAnimation = "";
        enum CharacterState
        {
            Walking,
            Idle,
            InAction,
            Jumping
        }

        public Character(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
            JoypadStates = new List<GamePadStateData>();

        }

        public void Update()
        {
            Position += velocity;
            GamePadState CurrentJoypadState = GamePad.GetState(PlayerIndex.One);
            KeyboardState CurrentKeyboardState = Keyboard.GetState();

            if (CurrentState == CharacterState.Idle || CurrentState == CharacterState.Walking)
            {
                GetSimpleInputs(CurrentKeyboardState);
                GetComboInputs(CurrentJoypadState);
            }

            EvaluateStatus();
        }

        float fActionStartTime = 0;
        float fActionDuration = 0;


        void EvaluateStatus()
        {
            if (CurrentState == CharacterState.Idle)
                ChangeAnimation("idle", false);

            if (CurrentState == CharacterState.InAction)
            {
                EvaluateActionProgress();
                ChangeAnimation(sCurrentActionAnimation, false);
            }


        }

        void EvaluateActionProgress()
        {
            float fPassedTime = SpriteManager.GameTime - fActionStartTime;

            if (fPassedTime >= fActionDuration)
            {
                if (sCurrentActionAnimation == "missile")
                {
                    Missile aduketMissile = AnimationSpecReader.PopulateAnimations<Missile>(@".\Content\Specs\ryu-aduket.json");
                    aduketMissile.Position.Y = Position.Y + 40;
                    aduketMissile.Position.X = Position.X + 60;
                    aduketMissile.fStartTime = SpriteManager.GameTime;
                    GameStatic.Missiles.Add(aduketMissile);
                }

                CurrentState = CharacterState.Idle;
            }
        }


        void GetSimpleInputs(KeyboardState CurrentKeyboardState)
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            if (iPlayerNo == 1)
            {
                if (CurrentKeyboardState.IsKeyDown(Keys.D))
                {
                    // high kick
                    CurrentState = CharacterState.InAction;
                    sCurrentActionAnimation = sHighKickAnimation;
                    fActionDuration = iHighKickDuration;
                    fActionStartTime = SpriteManager.GameTime;
                }
                else if (CurrentKeyboardState.IsKeyDown(Keys.C))
                {
                    // high punch
                    CurrentState = CharacterState.InAction;
                    sCurrentActionAnimation = sHighPunchAnimation;
                    fActionDuration = iHighPunchDuration;
                    fActionStartTime = SpriteManager.GameTime;
                }
                else if(CurrentKeyboardState.IsKeyDown(Keys.X) )
                {
                    CurrentState = CharacterState.InAction;
                    sCurrentActionAnimation = sEasyPunchAnimation;
                    fActionDuration = iEasyPunchDuration;
                    fActionStartTime = SpriteManager.GameTime;
                }
                else if (CurrentKeyboardState.IsKeyDown(Keys.Right))
                {
                    CurrentState = CharacterState.Walking;
                    Position.X += 3;
                    if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.None)
                    {
                        ChangeAnimation("walk-front", false);
                    }
                    else if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally)
                    {
                        ChangeAnimation("walk-back", false);
                    }
                }

                else if (CurrentKeyboardState.IsKeyDown(Keys.Left))
                {
                    CurrentState = CharacterState.Walking;
                    Position.X -= 3;
                    if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.None)
                    {
                        ChangeAnimation("walk-back", false);
                    }
                    else if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally)
                    {
                        ChangeAnimation("walk-front", false);
                    }

                }

                if (CurrentKeyboardState.IsKeyDown(Keys.Up) && hasJumped == false)
                {
                    Position.Y -= 10f;
                    velocity.Y -= 11F;
                    hasJumped = true;
                }
                if (hasJumped == true)
                {                  
                    velocity.Y += 0.5f ;
                    CurrentState = CharacterState.InAction;
                    sCurrentActionAnimation = sJumpUpAnimatiın;
                    fActionDuration = iJumpUpDuration;
                }
                if (Position.Y > 350)
                {                  
                    hasJumped = false;                  
                }
                if ( hasJumped == false)
                {   
                    velocity.Y = 0f;               
                }

                if (CurrentState == CharacterState.Walking && CurrentKeyboardState.IsKeyUp(Keys.Left) && CurrentKeyboardState.IsKeyUp(Keys.Right))
                {
                    CurrentState = CharacterState.Idle;
                }
            }

            if (iPlayerNo == 2)
            {
                if (gamePadState.Buttons.Y == ButtonState.Pressed)
                {
                    // high kick
                    CurrentState = CharacterState.InAction;
                    sCurrentActionAnimation = sHighKickAnimation;
                    fActionDuration = iHighKickDuration;
                    fActionStartTime = SpriteManager.GameTime;
                }
                else if (gamePadState.Buttons.A == ButtonState.Pressed)
                {
                    // high punch
                    CurrentState = CharacterState.InAction;
                    sCurrentActionAnimation = sHighPunchAnimation;
                    fActionDuration = iHighPunchDuration;
                    fActionStartTime = SpriteManager.GameTime;
                }
                else if (gamePadState.DPad.Right == ButtonState.Pressed)
                {
                    CurrentState = CharacterState.Walking;
                    Position.X += 3;
                    if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.None)
                    {
                        ChangeAnimation("walk", false);
                    }
                    else if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally)
                    {
                        ChangeAnimation("walk", false);
                    }
                }
                else if (gamePadState.DPad.Left == ButtonState.Pressed)
                {
                    CurrentState = CharacterState.Walking;
                    Position.X -= 3;
                    if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.None)
                    {
                        ChangeAnimation("walk", false);
                    }
                    else if (ActiveEffect == Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally)
                    {
                        ChangeAnimation("walk", false);
                    }
                }
                if (CurrentState == CharacterState.Walking && gamePadState.DPad.Left == ButtonState.Released && gamePadState.DPad.Right == ButtonState.Released)
                {
                    CurrentState = CharacterState.Idle;
                }

            }
        }

        void GetComboInputs(GamePadState CurrentJoypadState)
        {
            ComboNode NewNode = new ComboNode(CurrentJoypadState, SpriteManager.GameTime);
            if (!NewNode.Empty)
                JoypadStates.Add(new GamePadStateData(NewNode));

            EvaluateCombo();

        }

        float fLastTime = 0;
        void EvaluateCombo()
        {
            //remove olds from stack

            List<int> IndexToRemove = new List<int>();

            //if (JoypadStates.Count > 3)
            //{
            //    int a = 0;
            //}

            if (JoypadStates.Count > 0)
            {
                int LastChange = 0;
                for (int i = 0; i < JoypadStates.Count; i++)
                {
                    GamePadStateData NextData = JoypadStates[i];
                    if (NextData.State.fTime < SpriteManager.GameTime - fComboDuration)
                    {
                        IndexToRemove.Add(i);
                    }
                    else
                    {
                        if (i != LastChange && NextData.State.Compare(JoypadStates[LastChange].State))
                        {
                            IndexToRemove.Add(LastChange);

                        }
                        LastChange = i;
                    }
                }
            }
            for (int i = IndexToRemove.Count - 1; i > -1; i--)
            {
                JoypadStates.RemoveAt(IndexToRemove[i]);
            }


            List<ComboNode> CollectedNodes = new List<ComboNode>();

            //loop through combos and check
            for (int iComboCounter = 0; iComboCounter < AllCombos.Combos.Count; iComboCounter++)
            {

                int iDefinitionCounter = 0;
                ComboDefinition CurrentDefinition = AllCombos.Combos[iComboCounter];
                ComboNode FirstNode = CurrentDefinition.ComboStages[iDefinitionCounter];

                //loop through states
                for (int iStateCounter = 0; iStateCounter < JoypadStates.Count; iStateCounter++)
                {
                    //Console.WriteLine("combo: " + iComboCounter + " State count: " + JoypadStates.Count + " state: " + iStateCounter ); 
                    //try and find one matching first node
                    ComboNode CurrentStateNode = JoypadStates[iStateCounter].State;
                    if (CurrentStateNode.Compare(FirstNode) && CurrentDefinition.ComboStages.Count <= JoypadStates.Count - iStateCounter)
                    {
                        CollectedNodes = new List<ComboNode>();

                        ComboNode CurrentNode = CurrentDefinition.ComboStages[iDefinitionCounter];
                        //the current state matches the first node, run down and try to find a sequence match
                        //ComboNode CurrentNode = CurrentDefinition.ComboStages[iDefinitionCounter];
                        //ComboNode NextNode = CurrentDefinition.ComboStages[iDefinitionCounter + 1];
                        for (int iInternalStateCounter = iStateCounter; iInternalStateCounter < JoypadStates.Count; iInternalStateCounter++)
                        {
                            CurrentStateNode = JoypadStates[iInternalStateCounter].State;
                            ComboNode NextNode = CurrentDefinition.ComboStages[iDefinitionCounter];

                            if (CurrentStateNode.Compare(NextNode))
                            {
                                //we take another step
                                if (iDefinitionCounter > 1)
                                {
                                    int a = 1;
                                }
                                CollectedNodes.Add(CurrentStateNode);
                                iDefinitionCounter++;
                                CurrentNode = NextNode;

                            }
                            else
                            {
                                //leave, the sequence didnt work
                                break;
                            }

                        }


                        if (CollectedNodes.Count == CurrentDefinition.ComboStages.Count)
                        {
                            //check the timing
                            float fTimeTaken = CollectedNodes[CollectedNodes.Count - 1].fTime - CollectedNodes[0].fTime;

                            if (fTimeTaken <= CurrentDefinition.ComboTime)
                            {
                                Console.WriteLine("COMBO SUCCESS: " + CurrentDefinition.ComboName);
                                JoypadStates = new List<GamePadStateData>();
                                iDefinitionCounter = 0;
                                ExecuteCombo(CurrentDefinition.ComboName);
                            }
                            else
                            {

                                Console.WriteLine("Executed: " + CurrentDefinition.ComboName + " in " + fTimeTaken + " seconds, too late");
                            }
                        }
                    }
                }
            }
        }

        void ExecuteCombo(string sCombo)
        {
            if (sCombo == "missile")
            {
                CurrentState = CharacterState.InAction;
                sCurrentActionAnimation = sMissileAnimation;
                fActionDuration = iMissileDuration;
                fActionStartTime = SpriteManager.GameTime;
            }
            if (sCombo == "dapduket")
            {
                CurrentState = CharacterState.InAction;
                sCurrentActionAnimation = sDupduketAnimation;
                fActionDuration = iDupduketDuration;
                fActionStartTime = SpriteManager.GameTime;
            }
        }


        public void Hit()
        {

            CurrentState = CharacterState.InAction;
            sCurrentActionAnimation = sGotHitAnimation;
            fActionDuration = iGotHitDuration;
            fActionStartTime = SpriteManager.GameTime;
        }


    }
}
