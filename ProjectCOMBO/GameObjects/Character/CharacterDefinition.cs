﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCOMBO
{
    class CharacterDefinition
    {
        public string sName;
        public string sSpriteManagerJSON;
        public List<Moves> CharacterMoves;
            
    }

    class Moves
    {
        public string sMoveName;
        public string sMoveAnimation;
        public int iDuration;
    }
}
