﻿using HologramSpriteManager;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCOMBO
{
    class CharacterManager
    {
        public Character Player1;
        public Character Player2;
        public int Player1_Health = 100;
        public int Player2_Healt = 100;
        public int h_damage = -25;
        public int l_damage = -10;
        public bool bStartTime;
        public void LoadContent()
        {
            CharacterDefinition Player1Def;
            

            string sComboText = System.IO.File.ReadAllText(@".\Content\Specs\combos.json");
            string sPlayer1DefFile = System.IO.File.ReadAllText(@".\Content\Characters\CharRyu.json");

            Player1Def = JsonConvert.DeserializeObject<CharacterDefinition>(sPlayer1DefFile);

            Player1 = AnimationSpecReader.PopulateAnimations<Character>(Player1Def.sSpriteManagerJSON);
            Player1.AllCombos = JsonConvert.DeserializeObject<ComboDefinitions>(sComboText);
            Player1.Position = new Vector2(20,350);
            Player1.SetGeneralMoves(Player1Def.CharacterMoves);
            Player1.iPlayerNo = 1;


            CharacterDefinition Player2Def;
            string sPlayer2DefFile = System.IO.File.ReadAllText(@".\Content\Characters\CharSagat.json");
            Player2Def = JsonConvert.DeserializeObject<CharacterDefinition>(sPlayer2DefFile);
            Player2 = AnimationSpecReader.PopulateAnimations<Character>(Player2Def.sSpriteManagerJSON);
            Player2.AllCombos = JsonConvert.DeserializeObject<ComboDefinitions>(sComboText);
            Player2.Position = new Vector2(300, 280);
            Player2.SetGeneralMoves(Player2Def.CharacterMoves);
            Player2.iPlayerNo = 2;
        
        }

        public void Update()
        {
            Player1.Update();
            Player2.Update();

            if (Player1.CheckIntersect(Player2))
            {
                Collision currentcollision = Player1.CheckCollision(Player2);
                if (currentcollision.bCollided)
                { 
                    if(currentcollision.LocalGroup =="hit" && currentcollision.ExternalGroup == "body")
                    {
                        Player2.Hit();
                        
                    }
                    if (currentcollision.LocalGroup == "body" && currentcollision.ExternalGroup == "hit")
                    {
                        Player1.Hit();
                    }

                }
            }

            //flip player that is to the right
            if (Player1.Position.X > Player2.Position.X)
            {
                Player1.SetEffect(Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally);
                Player2.SetEffect(Microsoft.Xna.Framework.Graphics.SpriteEffects.None);            
            }
            else
            { 
                Player2.SetEffect(Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipHorizontally);
                Player1.SetEffect(Microsoft.Xna.Framework.Graphics.SpriteEffects.None);            
            }

            //check missiles
            for (int iMissileCounter = 0; iMissileCounter < GameStatic.Missiles.Count; iMissileCounter++)
            {
                Missile CurrentMissile = GameStatic.Missiles[iMissileCounter];
                if (Player2.CheckIntersect(CurrentMissile))
                {
                    Collision currentMissileCollision = Player2.CheckCollision(CurrentMissile);
                    if (currentMissileCollision.bCollided)
                    {
                        if (currentMissileCollision.LocalGroup == "body" && currentMissileCollision.ExternalGroup == "hit")
                        {
                            Player2.Hit();
                            
                        }
                    }

                }
            }       
        }

        public void Draw()
        {     
            Player1.Draw();
            Player2.Draw();
            
        }
    }
}
