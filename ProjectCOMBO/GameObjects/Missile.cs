﻿using HologramSpriteManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCOMBO
{
    class Missile : AnimatedSprite
    {
        public Missile(AnimatedSpriteSequences Meta)
            : base(Meta)
        {
         
        }

        float fAduketTime = 175;
        public float fStartTime = 0;

        public void Update()
        { 
        
            float fCurrentTime = SpriteManager.GameTime;
            float fTimePassed = fCurrentTime - fStartTime;
            if ((fTimePassed / fAduketTime ) > 1)
            {
                Position.X += 5;
            }
        }



    }
}
