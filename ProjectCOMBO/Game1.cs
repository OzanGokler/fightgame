﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using HologramSpriteManager;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Newtonsoft.Json;

namespace ProjectCOMBO
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        Texture2D imgBackground;
           
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            SpriteManager.ContentShell = Content;
            GameStatic.Initialise();
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            SpriteManager.GameTime = 0;

            base.Initialize();
        }


        //Character ryuAduket;
        public Song backMusic;
        protected override void LoadContent()
        {

            //backMusic = Content.Load<Song>("Music/start");
            //MediaPlayer.Play(backMusic);
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteManager.spriteBatch = new SpriteBatch(GraphicsDevice);
            imgBackground = Content.Load<Texture2D>("Sprites/Backgraund/starbucks");
            // TODO: use this.Content to load your game content here
            GameStatic.LoadContent();
            //ryuAduket.Position.Y = Player1.Position.Y + 40;
            //ryuAduket.Position.X = Player1.Position.X + 60;
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }




        protected override void Update(GameTime gameTime)
        {
            ///////////////////////////////////////////////////////////////
            
            // TODO: Add your update logic here
            SpriteManager.GameTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;


                       
            GameStatic.Update();
         



            base.Update(gameTime);
        }
        Rectangle rBackground = new Rectangle(0, 0, 800,480 );
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            // TODO: Add your drawing code here
            SpriteManager.spriteBatch.Begin();

            SpriteManager.spriteBatch.Draw(imgBackground, rBackground, Color.White);
           
            GameStatic.Draw();

            //ryuAduket.Draw();
            SpriteManager.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}